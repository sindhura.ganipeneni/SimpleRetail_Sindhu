package com.nTier.clients;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import com.nTier.reports.Report;
import com.retail.core.Item;
import com.retail.core.ItemProcessing;

public class Client {

	public static void main(String[] args) {
		Item i1=new Item(567321101987l,"CD - Pink Floyd, Dark Side of The Moon",19.99,0.58f,"AIR");
		Item i2=new Item(567321101986l,"CD - Beatles, Abbey Road",17.99,0.61f,"GROUND");
		Item i3=new Item(567321101985l,"CD - Queen, A Night at the Opera",20.49,0.55f,"AIR");
		Item i4=new Item(567321101984l,"CD - Michael Jackson, Thriller",23.88,0.50f,"GROUND");
		Item i5=new Item(467321101899l,"iPhone - Waterproof Case",9.75,0.73f,"AIR");
		Item i6=new Item(377321101878l,"iPhone - Headphones",17.25,3.21f,"GROUND");
		
		Item i7=new Item(367321101987l,"CD - Ed Sheeran, Shape of You",29.79,0.18f,"AIR");
		Item i8=new Item(367321101986l,"CD - Clean Bandit, Rockabye",5.99,0.21f,"GROUND");
		Item i9=new Item(367321101985l,"CD - Luis Fonsi, Despacito",12.4,0.55f,"AIR");
		Item i10=new Item(367321101984l,"CD - Sia, Cheap Thrills ",13.68,0.50f,"GROUND");
		Item i11=new Item(667321101899l,"iPhone - Handsfree",19.75,0.54f,"AIR");
		Item i12=new Item(677321101878l,"iPhone - Screen Protector",5.25,0.88f,"GROUND");
		
		HashSet<Item> hashSet=new HashSet<>();
		hashSet.add(i1);
		hashSet.add(i2);
		hashSet.add(i3);
		hashSet.add(i4);
		hashSet.add(i5);
		hashSet.add(i6);
		
		hashSet.add(i7);
		hashSet.add(i8);
		hashSet.add(i9);
		hashSet.add(i10);
		hashSet.add(i11);
		hashSet.add(i12);
		ArrayList<Item> itemList=new ArrayList<>();
		itemList.addAll(hashSet);
		/*Item.testDisplay(itemList);
		Collections.sort(itemList);
		Item.testDisplay(itemList);*/
		ItemProcessing.calculateShippingCost(itemList);
		//Item.testDisplay(itemList);
		Report r=new Report();
		r.generateReport(itemList);
		//System.out.println(display);
	}

}
