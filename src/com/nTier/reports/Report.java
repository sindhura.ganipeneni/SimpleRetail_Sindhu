package com.nTier.reports;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.retail.core.Item;
import com.retail.core.ItemProcessing;

public class Report {
public void generateReport(ArrayList<Item> items)
{	int length=items.size();
	List<Item> it=new ArrayList<>();
	int start=0;
	int end=6;
	while(length>=6)
	{
	LocalDateTime ldt=LocalDateTime.now();
	System.out.println("*************** Shipment Report *******************             "+ldt);
	System.out.println("\n"+"\n");
	System.out.format("%-30s%-50s%-30s%-30s%-30s%-30s", "UPC", "Description", "Price", "Weight", "Shipping Method", "Shipping Cost" );
	System.out.println();
	System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	Collections.sort(items,ItemProcessing.comparator);
	it= items.subList(start, end);
	double total=ItemProcessing.calculateTotal(it);
	for(Item item:it)
	{
		System.out.format("%-30s%-50s%-30s%-30s%-30s%-30s",item.getUpc(),item.getDescription(),item.getPrice(),item.getWeight(),item.getShipMethod(),item.getShippingCost());
		System.out.println();
	}
	System.out.println();
	System.out.println("TOTAL SHIPPING COST :                                                                                         "+total);
	length-=6;
	start=end;
	end+=6;
	}
	}
}
